import { useCallback, useEffect } from 'react'
import styles from '../styles/Home.module.css'
import { useWeb3React } from '@web3-react/core';
import { connector } from '../config/web3/index'

export default function Home() {

  const { activate, active, deactivate, account, error, chainId } = useWeb3React()

  const connect = useCallback(() => {
    activate(connector)
    localStorage.setItem('previouslyConnected', true)
  }, [activate])

  useEffect(() => {
    if(localStorage.getItem('previouslyConnected') === 'true')
      connect()
  }, [connect])

  const disconnect = () => {
    deactivate()
    localStorage.removeItem('previouslyConnected')
  }

  if(error) {
    return <p> se ha roto algo! </p>
  }


  return (
    <div className={styles.container}>
      <h1 className={styles.title}>Web3 demo app</h1>

      {
        active 
          ?  <>
                <button className={styles.button} onClick={disconnect}>Discconnect Wallet</button>
                <p>
                  You are connected to {chainId} network. <br/>
                  Your account: {account}
                </p>
            </> 
          : <button className={styles.button} onClick={connect}>Connect Wallet</button>
      }
      
    </div>
  )
}
